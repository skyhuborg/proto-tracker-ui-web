import * as jspb from "google-protobuf"

import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';

export class CameraConfig extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getEnabled(): boolean;
  setEnabled(value: boolean): void;

  getLocation(): string;
  setLocation(value: string): void;

  getProtocol(): string;
  setProtocol(value: string): void;

  getIp(): string;
  setIp(value: string): void;

  getPort(): number;
  setPort(value: number): void;

  getUsername(): string;
  setUsername(value: string): void;

  getPassword(): string;
  setPassword(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CameraConfig.AsObject;
  static toObject(includeInstance: boolean, msg: CameraConfig): CameraConfig.AsObject;
  static serializeBinaryToWriter(message: CameraConfig, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CameraConfig;
  static deserializeBinaryFromReader(message: CameraConfig, reader: jspb.BinaryReader): CameraConfig;
}

export namespace CameraConfig {
  export type AsObject = {
    name: string,
    enabled: boolean,
    location: string,
    protocol: string,
    ip: string,
    port: number,
    username: string,
    password: string,
  }
}

export class StorageConfig extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getLocation(): string;
  setLocation(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StorageConfig.AsObject;
  static toObject(includeInstance: boolean, msg: StorageConfig): StorageConfig.AsObject;
  static serializeBinaryToWriter(message: StorageConfig, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StorageConfig;
  static deserializeBinaryFromReader(message: StorageConfig, reader: jspb.BinaryReader): StorageConfig;
}

export namespace StorageConfig {
  export type AsObject = {
    name: string,
    location: string,
  }
}

export class Config extends jspb.Message {
  getPassword(): string;
  setPassword(value: string): void;

  getPasswordagain(): string;
  setPasswordagain(value: string): void;

  getHostname(): string;
  setHostname(value: string): void;

  getNodename(): string;
  setNodename(value: string): void;

  getCameraList(): Array<CameraConfig>;
  setCameraList(value: Array<CameraConfig>): void;
  clearCameraList(): void;
  addCamera(value?: CameraConfig, index?: number): CameraConfig;

  getStorageList(): Array<StorageConfig>;
  setStorageList(value: Array<StorageConfig>): void;
  clearStorageList(): void;
  addStorage(value?: StorageConfig, index?: number): StorageConfig;

  getConfigured(): boolean;
  setConfigured(value: boolean): void;

  getUuid(): string;
  setUuid(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Config.AsObject;
  static toObject(includeInstance: boolean, msg: Config): Config.AsObject;
  static serializeBinaryToWriter(message: Config, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Config;
  static deserializeBinaryFromReader(message: Config, reader: jspb.BinaryReader): Config;
}

export namespace Config {
  export type AsObject = {
    password: string,
    passwordagain: string,
    hostname: string,
    nodename: string,
    cameraList: Array<CameraConfig.AsObject>,
    storageList: Array<StorageConfig.AsObject>,
    configured: boolean,
    uuid: string,
  }
}

export class SetConfigReq extends jspb.Message {
  getConfig(): Config | undefined;
  setConfig(value?: Config): void;
  hasConfig(): boolean;
  clearConfig(): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetConfigReq.AsObject;
  static toObject(includeInstance: boolean, msg: SetConfigReq): SetConfigReq.AsObject;
  static serializeBinaryToWriter(message: SetConfigReq, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetConfigReq;
  static deserializeBinaryFromReader(message: SetConfigReq, reader: jspb.BinaryReader): SetConfigReq;
}

export namespace SetConfigReq {
  export type AsObject = {
    config?: Config.AsObject,
  }
}

export class SetConfigResp extends jspb.Message {
  getStatus(): number;
  setStatus(value: number): void;

  getMsg(): string;
  setMsg(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetConfigResp.AsObject;
  static toObject(includeInstance: boolean, msg: SetConfigResp): SetConfigResp.AsObject;
  static serializeBinaryToWriter(message: SetConfigResp, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetConfigResp;
  static deserializeBinaryFromReader(message: SetConfigResp, reader: jspb.BinaryReader): SetConfigResp;
}

export namespace SetConfigResp {
  export type AsObject = {
    status: number,
    msg: string,
  }
}

export class GetConfigReq extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetConfigReq.AsObject;
  static toObject(includeInstance: boolean, msg: GetConfigReq): GetConfigReq.AsObject;
  static serializeBinaryToWriter(message: GetConfigReq, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetConfigReq;
  static deserializeBinaryFromReader(message: GetConfigReq, reader: jspb.BinaryReader): GetConfigReq;
}

export namespace GetConfigReq {
  export type AsObject = {
  }
}

export class GetConfigResp extends jspb.Message {
  getConfig(): Config | undefined;
  setConfig(value?: Config): void;
  hasConfig(): boolean;
  clearConfig(): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetConfigResp.AsObject;
  static toObject(includeInstance: boolean, msg: GetConfigResp): GetConfigResp.AsObject;
  static serializeBinaryToWriter(message: GetConfigResp, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetConfigResp;
  static deserializeBinaryFromReader(message: GetConfigResp, reader: jspb.BinaryReader): GetConfigResp;
}

export namespace GetConfigResp {
  export type AsObject = {
    config?: Config.AsObject,
  }
}

export class GetIsConfiguredReq extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetIsConfiguredReq.AsObject;
  static toObject(includeInstance: boolean, msg: GetIsConfiguredReq): GetIsConfiguredReq.AsObject;
  static serializeBinaryToWriter(message: GetIsConfiguredReq, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetIsConfiguredReq;
  static deserializeBinaryFromReader(message: GetIsConfiguredReq, reader: jspb.BinaryReader): GetIsConfiguredReq;
}

export namespace GetIsConfiguredReq {
  export type AsObject = {
  }
}

export class GetIsConfiguredResp extends jspb.Message {
  getIsconfigured(): boolean;
  setIsconfigured(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetIsConfiguredResp.AsObject;
  static toObject(includeInstance: boolean, msg: GetIsConfiguredResp): GetIsConfiguredResp.AsObject;
  static serializeBinaryToWriter(message: GetIsConfiguredResp, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetIsConfiguredResp;
  static deserializeBinaryFromReader(message: GetIsConfiguredResp, reader: jspb.BinaryReader): GetIsConfiguredResp;
}

export namespace GetIsConfiguredResp {
  export type AsObject = {
    isconfigured: boolean,
  }
}

export class Event extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;
  hasCreatedAt(): boolean;
  clearCreatedAt(): void;

  getType(): string;
  setType(value: string): void;

  getSource(): string;
  setSource(value: string): void;

  getSensor(): string;
  setSensor(value: string): void;

  getDuration(): number;
  setDuration(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Event.AsObject;
  static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
  static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Event;
  static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
}

export namespace Event {
  export type AsObject = {
    id: string,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    type: string,
    source: string,
    sensor: string,
    duration: number,
  }
}

export class VideoEvent extends jspb.Message {
  getEventId(): string;
  setEventId(value: string): void;

  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;
  hasCreatedAt(): boolean;
  clearCreatedAt(): void;

  getUri(): string;
  setUri(value: string): void;

  getThumb(): string;
  setThumb(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VideoEvent.AsObject;
  static toObject(includeInstance: boolean, msg: VideoEvent): VideoEvent.AsObject;
  static serializeBinaryToWriter(message: VideoEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VideoEvent;
  static deserializeBinaryFromReader(message: VideoEvent, reader: jspb.BinaryReader): VideoEvent;
}

export namespace VideoEvent {
  export type AsObject = {
    eventId: string,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    uri: string,
    thumb: string,
  }
}

export class GetEventsReq extends jspb.Message {
  getLimit(): number;
  setLimit(value: number): void;

  getPage(): number;
  setPage(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetEventsReq.AsObject;
  static toObject(includeInstance: boolean, msg: GetEventsReq): GetEventsReq.AsObject;
  static serializeBinaryToWriter(message: GetEventsReq, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetEventsReq;
  static deserializeBinaryFromReader(message: GetEventsReq, reader: jspb.BinaryReader): GetEventsReq;
}

export namespace GetEventsReq {
  export type AsObject = {
    limit: number,
    page: number,
  }
}

export class GetEventsResp extends jspb.Message {
  getEventList(): Array<Event>;
  setEventList(value: Array<Event>): void;
  clearEventList(): void;
  addEvent(value?: Event, index?: number): Event;

  getTotal(): number;
  setTotal(value: number): void;

  getNpages(): number;
  setNpages(value: number): void;

  getPage(): number;
  setPage(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetEventsResp.AsObject;
  static toObject(includeInstance: boolean, msg: GetEventsResp): GetEventsResp.AsObject;
  static serializeBinaryToWriter(message: GetEventsResp, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetEventsResp;
  static deserializeBinaryFromReader(message: GetEventsResp, reader: jspb.BinaryReader): GetEventsResp;
}

export namespace GetEventsResp {
  export type AsObject = {
    eventList: Array<Event.AsObject>,
    total: number,
    npages: number,
    page: number,
  }
}

export class GetVideoEventsReq extends jspb.Message {
  getLimit(): number;
  setLimit(value: number): void;

  getPage(): number;
  setPage(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetVideoEventsReq.AsObject;
  static toObject(includeInstance: boolean, msg: GetVideoEventsReq): GetVideoEventsReq.AsObject;
  static serializeBinaryToWriter(message: GetVideoEventsReq, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetVideoEventsReq;
  static deserializeBinaryFromReader(message: GetVideoEventsReq, reader: jspb.BinaryReader): GetVideoEventsReq;
}

export namespace GetVideoEventsReq {
  export type AsObject = {
    limit: number,
    page: number,
  }
}

export class GetVideoEventsResp extends jspb.Message {
  getVideoList(): Array<VideoEvent>;
  setVideoList(value: Array<VideoEvent>): void;
  clearVideoList(): void;
  addVideo(value?: VideoEvent, index?: number): VideoEvent;

  getTotal(): number;
  setTotal(value: number): void;

  getNpages(): number;
  setNpages(value: number): void;

  getPage(): number;
  setPage(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetVideoEventsResp.AsObject;
  static toObject(includeInstance: boolean, msg: GetVideoEventsResp): GetVideoEventsResp.AsObject;
  static serializeBinaryToWriter(message: GetVideoEventsResp, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetVideoEventsResp;
  static deserializeBinaryFromReader(message: GetVideoEventsResp, reader: jspb.BinaryReader): GetVideoEventsResp;
}

export namespace GetVideoEventsResp {
  export type AsObject = {
    videoList: Array<VideoEvent.AsObject>,
    total: number,
    npages: number,
    page: number,
  }
}

