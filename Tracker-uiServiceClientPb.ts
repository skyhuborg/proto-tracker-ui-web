/**
 * @fileoverview gRPC-Web generated client stub for ui
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


import * as grpcWeb from 'grpc-web';

import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';

import {
  GetConfigReq,
  GetConfigResp,
  GetEventsReq,
  GetEventsResp,
  GetIsConfiguredReq,
  GetIsConfiguredResp,
  GetVideoEventsReq,
  GetVideoEventsResp,
  SetConfigReq,
  SetConfigResp} from './tracker-ui_pb';

export class UiClient {
  client_: grpcWeb.AbstractClientBase;
  hostname_: string;
  credentials_: null | { [index: string]: string; };
  options_: null | { [index: string]: string; };

  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: string; }) {
    if (!options) options = {};
    if (!credentials) credentials = {};
    options['format'] = 'text';

    this.client_ = new grpcWeb.GrpcWebClientBase(options);
    this.hostname_ = hostname;
    this.credentials_ = credentials;
    this.options_ = options;
  }

  methodInfoSetConfig = new grpcWeb.AbstractClientBase.MethodInfo(
    SetConfigResp,
    (request: SetConfigReq) => {
      return request.serializeBinary();
    },
    SetConfigResp.deserializeBinary
  );

  setConfig(
    request: SetConfigReq,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: SetConfigResp) => void) {
    return this.client_.rpcCall(
      this.hostname_ +
        '/ui.Ui/SetConfig',
      request,
      metadata || {},
      this.methodInfoSetConfig,
      callback);
  }

  methodInfoGetConfig = new grpcWeb.AbstractClientBase.MethodInfo(
    GetConfigResp,
    (request: GetConfigReq) => {
      return request.serializeBinary();
    },
    GetConfigResp.deserializeBinary
  );

  getConfig(
    request: GetConfigReq,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: GetConfigResp) => void) {
    return this.client_.rpcCall(
      this.hostname_ +
        '/ui.Ui/GetConfig',
      request,
      metadata || {},
      this.methodInfoGetConfig,
      callback);
  }

  methodInfoGetIsConfigured = new grpcWeb.AbstractClientBase.MethodInfo(
    GetIsConfiguredResp,
    (request: GetIsConfiguredReq) => {
      return request.serializeBinary();
    },
    GetIsConfiguredResp.deserializeBinary
  );

  getIsConfigured(
    request: GetIsConfiguredReq,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: GetIsConfiguredResp) => void) {
    return this.client_.rpcCall(
      this.hostname_ +
        '/ui.Ui/GetIsConfigured',
      request,
      metadata || {},
      this.methodInfoGetIsConfigured,
      callback);
  }

  methodInfoGetEvents = new grpcWeb.AbstractClientBase.MethodInfo(
    GetEventsResp,
    (request: GetEventsReq) => {
      return request.serializeBinary();
    },
    GetEventsResp.deserializeBinary
  );

  getEvents(
    request: GetEventsReq,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: GetEventsResp) => void) {
    return this.client_.rpcCall(
      this.hostname_ +
        '/ui.Ui/GetEvents',
      request,
      metadata || {},
      this.methodInfoGetEvents,
      callback);
  }

  methodInfoGetVideoEvents = new grpcWeb.AbstractClientBase.MethodInfo(
    GetVideoEventsResp,
    (request: GetVideoEventsReq) => {
      return request.serializeBinary();
    },
    GetVideoEventsResp.deserializeBinary
  );

  getVideoEvents(
    request: GetVideoEventsReq,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: GetVideoEventsResp) => void) {
    return this.client_.rpcCall(
      this.hostname_ +
        '/ui.Ui/GetVideoEvents',
      request,
      metadata || {},
      this.methodInfoGetVideoEvents,
      callback);
  }

}

