"use strict";
/**
 * @fileoverview gRPC-Web generated client stub for ui
 * @enhanceable
 * @public
 */
exports.__esModule = true;
// GENERATED CODE -- DO NOT EDIT!
var grpcWeb = require("grpc-web");
var tracker_ui_pb_1 = require("./tracker-ui_pb");
var UiClient = /** @class */ (function () {
    function UiClient(hostname, credentials, options) {
        this.methodInfoSetConfig = new grpcWeb.AbstractClientBase.MethodInfo(tracker_ui_pb_1.SetConfigResp, function (request) {
            return request.serializeBinary();
        }, tracker_ui_pb_1.SetConfigResp.deserializeBinary);
        this.methodInfoGetConfig = new grpcWeb.AbstractClientBase.MethodInfo(tracker_ui_pb_1.GetConfigResp, function (request) {
            return request.serializeBinary();
        }, tracker_ui_pb_1.GetConfigResp.deserializeBinary);
        this.methodInfoGetIsConfigured = new grpcWeb.AbstractClientBase.MethodInfo(tracker_ui_pb_1.GetIsConfiguredResp, function (request) {
            return request.serializeBinary();
        }, tracker_ui_pb_1.GetIsConfiguredResp.deserializeBinary);
        this.methodInfoGetEvents = new grpcWeb.AbstractClientBase.MethodInfo(tracker_ui_pb_1.GetEventsResp, function (request) {
            return request.serializeBinary();
        }, tracker_ui_pb_1.GetEventsResp.deserializeBinary);
        this.methodInfoGetVideoEvents = new grpcWeb.AbstractClientBase.MethodInfo(tracker_ui_pb_1.GetVideoEventsResp, function (request) {
            return request.serializeBinary();
        }, tracker_ui_pb_1.GetVideoEventsResp.deserializeBinary);
        if (!options)
            options = {};
        if (!credentials)
            credentials = {};
        options['format'] = 'text';
        this.client_ = new grpcWeb.GrpcWebClientBase(options);
        this.hostname_ = hostname;
        this.credentials_ = credentials;
        this.options_ = options;
    }
    UiClient.prototype.setConfig = function (request, metadata, callback) {
        return this.client_.rpcCall(this.hostname_ +
            '/ui.Ui/SetConfig', request, metadata || {}, this.methodInfoSetConfig, callback);
    };
    UiClient.prototype.getConfig = function (request, metadata, callback) {
        return this.client_.rpcCall(this.hostname_ +
            '/ui.Ui/GetConfig', request, metadata || {}, this.methodInfoGetConfig, callback);
    };
    UiClient.prototype.getIsConfigured = function (request, metadata, callback) {
        return this.client_.rpcCall(this.hostname_ +
            '/ui.Ui/GetIsConfigured', request, metadata || {}, this.methodInfoGetIsConfigured, callback);
    };
    UiClient.prototype.getEvents = function (request, metadata, callback) {
        return this.client_.rpcCall(this.hostname_ +
            '/ui.Ui/GetEvents', request, metadata || {}, this.methodInfoGetEvents, callback);
    };
    UiClient.prototype.getVideoEvents = function (request, metadata, callback) {
        return this.client_.rpcCall(this.hostname_ +
            '/ui.Ui/GetVideoEvents', request, metadata || {}, this.methodInfoGetVideoEvents, callback);
    };
    return UiClient;
}());
exports.UiClient = UiClient;
